<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Simpleblog extends CI_Controller {

	/*
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/home
	 *	- or -  
	 * 		http://example.com/index.php/home/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html /
	 */

	function index()
	{	
	
		//$this->load->library('session');
		$this->load->theme('demo');
		
	//echo 'second function';
	$this->load->model('content_model');
	
	
	if ($content_data = $this->content_model->GetContent()) {
	$data['content'] = $content_data; 
		
		$this->load->view('my_view',$data);
		}
	
	}
	
	public function add_post (){
	$this->load->theme('demo');
	
	$this->load->view('form_view');
	
	}
	
	
	public function create (){
	
	$title = $this->input->post('title');
	$body = $this->input->post('body');
	$this->load->model('test_model');
	$this->test_model->add_new_post($title, $body);
	$this->load->theme('demo');
	$this->load->view('success');
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
?>